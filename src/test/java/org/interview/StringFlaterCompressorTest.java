package org.interview;

/**
 * Unit test for compression/decompression operations
 *
 * @see org.interview.BaseCompressorTest default methods with @Test annotation
 */
public class StringFlaterCompressorTest extends BaseCompressorTest {

    /**
     * Default string to test
     */
    private final String INPUT = "AAAAANNNMMMMYYYYuuuuUUUUaaaarWWLLLLJ888DDDDDDDDD";

    private static final StringCompressor INSTANCE = new StringFlaterCompressor();

    @Override
    StringCompressor getCompressorImpl() {
        return INSTANCE;
    }

    @Override
    String getStringTestInstance() {
        return INPUT;
    }
}
