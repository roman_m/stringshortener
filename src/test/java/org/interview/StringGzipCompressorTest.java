package org.interview;

/**
 * Unit test for compression/decompression operations
 *
 * @see org.interview.BaseCompressorTest default methods with @Test annotation
 */
public class StringGzipCompressorTest extends BaseCompressorTest {

    /**
     * Not effective for short strings
     */
    private final String INPUT = "AAAAANNNMMMMYYYYuuuuUUUUaaaarWWLLLLJDDDDDDDDD" +
            "AAAAANNNMMMMYYYYuuuuUUUUaaaarWWLLLLJDDDDDDDDD" +
            "AAAAANNNMMMMYYYYuuuuUUUUaaaarWWLLLLJDDDDDDDDD";

    private static final StringCompressor INSTANCE = new StringGzipCompressor();

    @Override
    StringCompressor getCompressorImpl() {
        return INSTANCE;
    }

    @Override
    String getStringTestInstance() {
        return INPUT;
    }
}
