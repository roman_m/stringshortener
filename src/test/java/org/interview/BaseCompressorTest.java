package org.interview;

import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertTrue;

/**
 * Base test for compression implementation classes
 */
public abstract class BaseCompressorTest {

    /**
     * @return instance if {@link StringCompressor} implementation
     */
    abstract StringCompressor getCompressorImpl();

    /**
     * @return a string to run test with
     *   different for some implementation to reveal pros/cons of approaches
     */
    abstract String getStringTestInstance();

    /**
     * Verify that operations are safe (null & empty check)
     */
    @Test
    public void testNullAndEmptyStringAsInputs() throws Exception {
        String compressNull    = getCompressorImpl().compress(null);
        String compressEmpty   = getCompressorImpl().compress("");
        String decompressNull  = getCompressorImpl().decompress(null);
        String decompressEmpty = getCompressorImpl().decompress("");

        String msg = "should be empty string";
        assertEquals("", compressNull, msg);
        assertEquals("", compressEmpty, msg);
        assertEquals("", decompressNull, msg);
        assertEquals("", decompressEmpty, msg);
    }

    /**
     * Encoding test - effective result
     */
    @Test
    public void testCompressionEffective() throws Exception {
        String output = getCompressorImpl().compress(getStringTestInstance());

        String msg = "Compress operation should decrease string length for original";
        assertTrue(getStringTestInstance().length() > output.length(), msg);
    }

    /**
     * Ineffective compression - non-repeatable characters will produce longer string
     */
    @Test
    public void testCompressionIneffective() throws Exception {
        String input = "ABCDEFGHIJK";
        String output = getCompressorImpl().compress(input);

        String msg = "Compress operation should not decrease string length for original as symbols non-repeatable";
        assertTrue(input.length() <= output.length(), msg);
    }

    /**
     * Test decompression
     */
    @Test
    public void testDecompression() throws Exception {
        String actual = getCompressorImpl().decompress(getCompressorImpl().compress(getStringTestInstance()));
        assertEquals(getStringTestInstance(), actual, "Strings are not equals after decompression");
    }

    /**
     * Test compression idempotency - same result regardless how many times method called
     */
    @Test
    public void testCompressionIdempotency() throws Exception {
        String output1 = getCompressorImpl().compress(getStringTestInstance());
        String output2 = getCompressorImpl().compress(getStringTestInstance());

        String msg = "Compressed outputs should produce idempotent results";
        assertEquals(output1, output2, msg);
    }

    /**
     * Test decompression idempotency - same result regardless how many times method called
     */
    @Test
    public void testDecompressionIdempotency() throws Exception {
        String compressed = getCompressorImpl().compress(getStringTestInstance());
        String output1 = getCompressorImpl().decompress(compressed);
        String output2 = getCompressorImpl().decompress(compressed);

        String msg = "Decompressed outputs should produce idempotent results";
        assertEquals(output1, output2, msg);
    }
}
