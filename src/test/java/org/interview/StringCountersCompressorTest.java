package org.interview;

/**
 * Unit test for compression/decompression operations based on counting symbols
 * Example:
 * AAABBBCCC -> A3B3C2
 *
 * @see org.interview.BaseCompressorTest default methods with @Test annotation
 */
public class StringCountersCompressorTest extends BaseCompressorTest {

    /**
     * String input to test
     *
     * Note: without any numbers intentionally for this implementation
     */
    private static final String INPUT = "AAAAANNNMMMMYYYYuuuuUUUUaaaarWWLLLLJDDDDDDDDD";

    private static final StringCompressor INSTANCE = new StringCountersCompressor();

    @Override
    StringCompressor getCompressorImpl() {
        return INSTANCE;
    }

    @Override
    String getStringTestInstance() {
        return INPUT;
    }
}
