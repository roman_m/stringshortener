package org.interview;

import java.io.*;
import java.nio.charset.StandardCharsets;
import java.util.zip.*;

/**
 * JDK's Deflater/Inflater streams implementation of strings compression/decompression operations
 *
 * Note: To manage compression strategy/level see:
 * {@link Deflater#Deflater(int)}
 * {@link DeflaterOutputStream#DeflaterOutputStream(OutputStream, Deflater)}
 */
public class StringFlaterCompressor implements StringCompressor {

    public String compress(String input) throws Exception {
        if (input == null || input.isEmpty()) {
            return "";
        }

        byte[] bytes = compress(input.getBytes(StandardCharsets.ISO_8859_1));
        String output = new String(bytes, 0, bytes.length, StandardCharsets.ISO_8859_1);

        System.out.println("Compressed String :" + output + "\n Size :" + output.length());
        System.out.println("Original String :" + input + "\n Size :" + input.length());

        return output;
    }

    public String decompress(String input) throws Exception {
        if (input == null || input.isEmpty()) {
            return "";
        }

        byte[] bytes = decompress(input.getBytes(StandardCharsets.ISO_8859_1));
        String output = new String(bytes, 0, bytes.length, StandardCharsets.ISO_8859_1);

        System.out.println("Decompressed String :" + output + "\n Size :" + output.length());
        System.out.println("Original String :" + input + "\n Size :" + input.length());

        return output;
    }

    private static byte[] compress(byte[] bytes) throws IOException {
        ByteArrayOutputStream os = new ByteArrayOutputStream();
        try (DeflaterOutputStream dos = new DeflaterOutputStream(os)) {
            dos.write(bytes);
        }
        return os.toByteArray();
    }

    private static byte[] decompress(byte[] compressed) throws IOException {
        ByteArrayOutputStream os = new ByteArrayOutputStream();
        try (OutputStream ios = new InflaterOutputStream(os)) {
            ios.write(compressed);
        }

        return os.toByteArray();
    }
}
