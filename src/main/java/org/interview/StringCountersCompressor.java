package org.interview;

/**
 * Counters or Run-Length encoding (RLE)
 *
 * Note: will not work with proposed string, because of '888' presence
 * Added just in case ( YAGNI violation :) )
 */
public class StringCountersCompressor implements StringCompressor {

    public String compress(String input) throws Exception {
        if (input == null || input.isEmpty()) {
            return "";
        }

        StringBuilder result = new StringBuilder();
        char[] chars = input.toCharArray();
        char current = chars[0];
        int counter = 1;

        for (int i = 1; i < chars.length; i++) {
            if (current != chars[i]) {
                if (counter > 1) result.append(counter);
                result.append(current);
                current = chars[i];
                counter = 1;
            } else {
                counter++;
            }
        }
        if (counter > 1) {
            result.append(counter);
        }
        result.append(current);

        String outputString = result.toString();

        System.out.println("Compressed String :" + outputString + "\n Size :" + outputString.length());
        System.out.println("Original String :" + input + "\n Size :" + input.length());

        return outputString;
    }

    public String decompress(String input) throws Exception {
        if (input == null || input.isEmpty()) {
            return "";
        }

        StringBuilder result = new StringBuilder();

        char[] chars = input.toCharArray();
        StringBuilder tempDigits = new StringBuilder();
        boolean isPreviousDigit = false;
        for (char current: chars) {
            if (!Character.isDigit(current)) {
                if (isPreviousDigit) {
                    String multipleString = repeatChar(current, Integer.parseInt(tempDigits.toString()));
                    result.append(multipleString);
                    isPreviousDigit = false;
                    tempDigits = new StringBuilder();
                } else {
                    result.append(current);
                }
            } else {
                tempDigits.append(current);
                isPreviousDigit = true;
            }
        }

        String outputString = result.toString();

        System.out.println("Decompressed String :" + outputString + "\n Size :" + outputString.length());
        System.out.println("Original String :" + input + "\n Size :" + input.length());

        return outputString;
    }

    private static String repeatChar(char character, int times) {
        StringBuilder sb = new StringBuilder();
        for (int i = 0; i < times; i++) {
            sb.append(character);
        }
        return sb.toString();
    }
}
