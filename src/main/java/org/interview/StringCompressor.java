package org.interview;

/**
 * Base interface for compressing-related operations with strings
 */
public interface StringCompressor {

    /**
     * @param input Nullable string to compress
     * @return compressed string or empty if null
     * @throws Exception
     */
    String compress(String input) throws Exception;

    /**
     * @param input Nullable string to decompress
     * @return decompressed string or empty if null
     * @throws Exception
     */
    String decompress(String input) throws Exception;
}
