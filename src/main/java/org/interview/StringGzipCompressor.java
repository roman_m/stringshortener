package org.interview;

import java.io.*;
import java.nio.charset.StandardCharsets;
import java.util.Base64;
import java.util.zip.*;

/**
 * Gzip+Base64 implementation of strings compression/decompression operations
 */
public class StringGzipCompressor implements StringCompressor {

    public String compress(String input) throws Exception {
        if (input == null || input.isEmpty()) {
            return "";
        }

        ByteArrayOutputStream obj = new ByteArrayOutputStream();
        GZIPOutputStream gzip = new GZIPOutputStream(obj);
        gzip.write(input.getBytes(StandardCharsets.ISO_8859_1));
        gzip.close();

        String output = Base64.getEncoder().encodeToString(obj.toByteArray());

        System.out.println("Compressed String :" + output + "\n Size :" + output.length());
        System.out.println("Original String :" + input + "\n Size :" + input.length());
        return output;
    }

    public String decompress(String input) throws Exception {
        if (input == null || input.isEmpty()) {
            return "";
        }

        GZIPInputStream gis = new GZIPInputStream(new ByteArrayInputStream(Base64.getDecoder().decode(input)));
        BufferedReader br = new BufferedReader(new InputStreamReader(gis, StandardCharsets.ISO_8859_1));
        StringBuilder sb = new StringBuilder();
        String line;
        while ((line = br.readLine()) != null) {
            sb.append(line);
        }
        String output = sb.toString();

        System.out.println("Decompressed String :" + output + "\n Size :" + output.length());
        System.out.println("Original String :" + input + "\n Size :" + input.length());
        return output;
    }
}
