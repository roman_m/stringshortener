
# Task description
Consider the following string

### "AAAAANNNMMMMYYYYuuuuUUUUaaaarWWLLLLJ888DDDDDDDDD"

Write a program, in Java, that shortens the string so it can be stored in fewer bytes,
and when required the shortened string can be restored to its original state.

The exercise should include unit tests.

Any JDK 1.8 or later is permitted.
The use of 3rd party libraries (apart from those needed for unit tests) should be avoided.

# Implementation notes

Added several implementations to illustrate how task can be solved with different approaches:
1. Counters or Run-Length encoding (RLE).
2. Zip-based implementation Deflater/Inflater JDK APIs for compressing, better than RLE.
3. GZip + Base64, most common approach in web.

# Summary
1. RLE will not work with proposed string due digits' presence.
2 & 3. Both use zip-compression, 2nd - use less space

The most effective and flexible approach is to use Deflator/Inflator APIs:
Pros:
- Flexible, allows configuring compression level and strategy
- Uses less memory than gzip-based solution
- JDK based solution
- Relatively fast

# How to run
Run following command using CLI:
```mvn test```
or execute tests manually using your IDE.
